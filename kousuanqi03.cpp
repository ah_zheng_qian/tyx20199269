#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int all();
int one();
int two();
int three();
int four();
int five();
int one_sth();
int two_sth();
int three_sth();
int main(void)
{
	int a;
	printf("-----口算生成器-----\n");
	printf("欢迎使用口算生成器\n");
	printf("帮助信息\n");
	printf("您需要输入命令代号进行操作，且\n");
	printf("一年级题目为不超过十位的加减法;\n");
	printf("二年级题目为不超过百位的乘除法\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n\n\n");
	while(a!=5)
	{
	
		printf("操作列表；\n");
		printf("1)一年级   2）二年级  3）三年级\n");
		printf("4)帮助   5）退出程序\n");
		printf("请输入操作：\n");
		scanf("%d",&a);
	    printf("执行操作......\n\n\n");
	    switch(a)
	    {
	    	case 1:one();break;
	    	case 2:two();break;
	    	case 3:three();break;
	    	case 4:four();break;
	    	case 5:five();break;
	    	default:all();
		}
	}
	return 0;
}
one()
{
    printf("现在是一年级题目：\n");
    one_sth();
    printf("执行完毕\n\n\n");
}
two()
{
	printf("现在是二年级题目：\n");
	two_sth();
	printf("执行完毕\n\n\n");
}
three()
{
	printf("现在是三年级题目\n");
	three_sth();
	printf("执行完毕\n\n\n");
	
}
four()
{
	printf("帮助信息：\n");
	printf("您需要输入命令代号来进行操作，且\n");
	printf("一年级题目为不超过十位的加减法\n");
	printf("二年级题目为不超过百位的乘除法;\n");
	printf("三年级题目为不超过百位的加减乘除混合题目.\n");
	printf("执行完毕\n\n\n");
}
five()
{
	printf("程序结束，欢迎下次使用\n按任意键结束.....");
}
all()
{
	printf("Error!\n错误操作指令，请重新输入\n\n\n");
}
one_sth()
{
	int i,n;
	char flag[2]={'+','-'};
	scanf("%d",&n);
	time_t t;
	srand((unsigned) time(&t));
	for(i=0;i<n;i++)
	  printf("%d %c %2d=_\n",rand() % 10,flag[rand() % 2],rand() % 10);
}
two_sth()
{
	int i,n;
	char flag[2]={'*','/'};
	scanf("%d",&n);
	time_t t;
	srand((unsigned) time(&t));
	for(i=0;i<n;i++)
	  printf("%2d %c %2d=_\n",rand() % 99,flag[rand() % 2],rand() % 99);
	  
}
three_sth()
{
	int i,n;
	char flag[4]={'+','-','*','/'};
	scanf("%d",&n);
	time_t t;
	srand((unsigned) time(&t));
	for(i=0;i<n;i++) 
	{
		printf("%2d %c %2d %c %2d=_\n",rand() % 99,flag[rand() % 4],rand() % 99,flag[rand() % 4],rand() % 99);
	}
}

